#!/bin/sh

./node_modules/.bin/graphql query -e ethql compare_schemas/availableQueries.gql -o queries | jq -f compare_schemas/array_to_dict.jq > ethql-queries.json
./node_modules/.bin/graphql query -e geth compare_schemas/availableQueries.gql -o queries | jq -f compare_schemas/array_to_dict.jq > geth-queries.json

./node_modules/.bin/jsondiffpatch ethql-queries.json geth-queries.json
