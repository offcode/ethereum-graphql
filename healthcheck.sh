#!/bin/sh

curl \
  -X POST \
  -H "Content-Type: application/json" \
  --data '{ "query": "{ syncing  { highestBlock } }" }' \
  http://127.0.0.1:8547/graphql