

fromAddress = eth.accounts[0]
nonce = web3.eth.getTransactionCount(fromAddress)
console.log("nonce", nonce)
raw = {
    from: fromAddress,
    to: eth.accounts[1],
    //value: web3.utils.toHex(web3.utils.toWei(0.003, "ether")),
    value: 22,
    gasPrice: 190000,
    gas: 200000,
    nonce: web3.toHex(nonce)
}

signed = web3.eth.signTransaction(raw)

function sendSigned(signed) {
    eth.sendRawTransaction(signed.raw, function(error, success) {
        if (error) {
            console.warn( error)
        }
        else {
            console.log('Success', success)
        }
    })
}