Based on Pantheon's [test blocks](https://github.com/PegaSysEng/pantheon/tree/master/testutil/src/main/resources) and
[test suites](https://github.com/PegaSysEng/pantheon/tree/master/ethereum/graphql/src/test/resources/tech/pegasys/pantheon/ethereum/graphql)

# Prerequisites

- Geth
- Node.js

# Run

Set up your node environment by running `npm install`.

Open a terminal and run `./init-and-run.sh`. Wait until it prints "GraphQL endpoint opened".  It takes only a few seconds.

Open another terminal and run `node main.test.js eth/*.json`. It'll run all the tests.


# Failing tests

Here are the tests that fail with Geth:
- blocks_byWrongRange: Check spec for empty range -> spec says nothing
- gasPrice: Explore how it's calculated. Gas price expected: 0x10, got: 0x1
- getBalance_invalidAccountBlockNumber: What does it check? It's a valid account. But it may be a good test to check a balance for an invalid account as per `web3.isAddress()`. Check spec for non-existant account. -> spec says nothing
- getBalance_invalidAccountLatest: same as above
- getBalance_toobig_bn: Checks the same as getBlock_byNumberInvalid, probably unneeded
- getBalance_without_addr: Different error messages -> Unify missing field error message.
- getBlock_byHashInvalid: Got empty data, expected error. Check spec for non-existent block hash.
- getBlock_byNumberInvalid: Expected error, got empty data. Check spec for too high block number.
- getBlock_wrongParams: different error messages.  Probably no need for so many return fields.
- getTransactionReceipt: difference between the returned type of `gas` and `cumulativeGasUsed`.  Also: `createdContracts` is expected to be null in the json, but Geth return a value.
- getTransaction_byHash: `status` is expected to be null in the json (meaning it wasn't mined yet), but Geth returns 0 meaning it failed.
- pending: return type of estimateGas differ
- sendRawTransaction_contractCreation: Geth complains about insufficient funds. Debug it
- sendRawTransaction_messageCall: Debug why insufficient funds in Geth
- sendRawTransaction_nonceTooLow: different error messages
- sendRawTransaction_transferEther: Debug why insufficient funds in Geth?
- sendRawTransaction_unsignedTransaction: different error messages
- syncing: Fix geth run script to make it sync

Issues for Geth:
- [Return error location](https://github.com/ethereum/go-ethereum/issues/20042)
- [Return error for invalid address](https://github.com/ethereum/go-ethereum/issues/20041)
- [estimateGas, gas, accumulatedGas, status should all return Long](https://github.com/ethereum/go-ethereum/issues/20040)

Issues for Pantheon:
- Query account within block, no top level account