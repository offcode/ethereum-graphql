#!/bin/sh

rm -rf testdata
mkdir testdata
shopt -s expand_aliases
alias docker_besu='docker run -p 8547:8547 --mount type=bind,source="$(pwd)"/init,target=/init  --mount type=bind,source="$(pwd)"/testdata,target=/testdata hyperledger/besu --data-path /testdata '
docker_besu --genesis-file /init/testGenesis.json blocks import --from=/init/testBlockchain.blocks
docker_besu --discovery-enabled=false --graphql-http-enabled=true --genesis-file /init/testGenesis.json