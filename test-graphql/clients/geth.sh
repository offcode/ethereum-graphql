#!/bin/sh

rm -rf testdata
mkdir testdata
shopt -s expand_aliases
alias docker_geth='docker run -p 8547:8547 --mount type=bind,source="$(pwd)"/init,target=/init  --mount type=bind,source="$(pwd)"/testdata,target=/testdata  ethereum/client-go:stable --datadir /testdata '
docker_geth init /init/testGenesis.json
docker_geth --gcmode=archive import /init/testBlockchain.blocks
docker_geth --nodiscover --graphql --graphql.addr 0.0.0.0