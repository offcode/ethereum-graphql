#!/bin/sh

rm -rf testdata
mkdir testdata
shopt -s expand_aliases
alias docker_trinity='docker run -p 8547:8547 --mount type=bind,source="$(pwd)"/init,target=/init  --mount type=bind,source="$(pwd)"/testdata,target=/testdata  ethereum/trinity --data-dir /testdata '
docker_trinity --genesis /init/testGenesis.json