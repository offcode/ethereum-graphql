#!/bin/sh

FILES=pantheon/*.json
for f in $FILES
do
base=$(echo $f | sed -e 's|^.*/eth_||g' -e 's|^.*graphql_||' -e 's/.json//')
    echo $base
    cat $f | \
        jq -r .request | \
        sed -e "s/^/query ${base} /" | \
        sed -e "s/^.*mutation /mutation ${base} /" | \
        ./node_modules/.bin/prettier --parser graphql > eth/$base.graphql
    cat $f | jq -r .response | ./node_modules/.bin/prettier --parser json > eth/$base.json
done

