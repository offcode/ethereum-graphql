const argv = require('yargs').argv
const fs = require('fs')
const { request } = require('graphql-request')
const assert = require('chai').assert
const addContext = require('mochawesome/addContext')


const tests_or_files = argv._.slice(1) || ['./eth/unknownAccounts']


describe('all', () => {
    tests_or_files.forEach(tname => {
        if (tname.endsWith('.json')) {
            tname = tname.slice(0, - ".json".length)
        }
        it(tname, async function() {
            const expected = JSON.parse(fs.readFileSync(`${tname}.json`, "utf8"))
            const query = fs.readFileSync(`${tname}.graphql`, "utf8")
            addContext(this, query)
            try {
                const data = await request('http://localhost:8547/graphql', query)
                result = {data}
            } catch (e) {
                errors = e.response ? e.response.errors : e.errors
                result = {errors}
            }
            assert.deepEqual(result, expected)
        })
    })
})