---
author: Adam Schmideg
---

# Understanding Ethereum with GraphQL: from zero to getting the account balance

In this series we'll explore the basic concepts of Ethereum using GraphQL.
We'll also touch on what a smart contract is and how to interact with it, but
we won't get into details how to write one. You don't need any knowledge of
blockchain or Ethereum in particular, neither of GraphQL.

A blockchain is a single database with many copies over the world. There is
one Bitcoin database, one Ethereum database, like there's one internet, one
Jupiter, one God. But there are thousands of copies of the Bitcoin database,
thousands of copies of the Ethereum database, and so on.

These databases have different features, but deep down they are all
blockchains. A blockchain is a simple database that map big random integers
(20 bytes long to be precise) to numerical values. You can change the values, but
you can't change the keys, apart from adding a new random key. The database
looks like this

- 966588469268559010541288244128342317224451555083 => 72.4
- 562824648592572320847590478409708627183864037263 => 0.00373
- ...

It's dumber than the dumbest key-value store. So what's the big fuss about
it? We'll get there, bear with me.

## Setting the stage

All we need is an Ethereum client that supports GraphQL queries. An Ethereum
client is a program that can copy the whole Ethereum database (in a smart
way) and provide you with some interfaces you can interact with. We're
interested in the GraphQL interface now. I'm going to use Geth, because
that's what I'm most familiar with.

First of all, [install Geth](https://geth.ethereum.org/install-and-build/Installing-Geth).

Download [a genesis json](https://gitlab.com/offcode/ethereum-graphql/blob/master/test-graphql/testGenesis.json) that will be used to create the initial state.

Download a [small chain](https://gitlab.com/offcode/ethereum-graphql/blob/master/test-graphql/testBlockchain.blocks).
It's a small binary file that contains all data of an Ethereum blockchain.  We're going to use it, because Geth reads it up fast and you don't have to wait until it syncs with other nodes.  For a real-world usage, you'll have to sync with an existing testnet or the mainnet; more on that later.

Run these commands
```sh
# Get rid of any trash left by a previous run
rm -rf testdata
# Initialize the chain with the json file you downloaded
geth --datadir testdata init testGenesis.json
# Import the blocks file you downloaded
geth --datadir testdata --gcmode=archive import testBlockchain.blocks
# Launch Geth
geth --datadir testdata --graphql
```

Geth has a plethora of command line arguments you can explore with `geth -h`.
 The `--graphql` switch does two things. It starts a
GraphQL server you can access at http://127.0.0.1:8547/graphql by default if
you're using an external tool to query it. It also gives us a Graph*i*QL web
interface at http://127.0.0.1:8547 -- note the *i* in the middle, it denotes
that [GraphiQL is an in-browser IDE](https://github.com/graphql/graphiql).
You don't have to install anything to use GraphQL, you'll only need Geth and
a browser.

Now point your browser to http://127.0.0.1:8547 and execute this query to
check everything is set up correctly,

```graphql
query is_syncing {
  syncing {
    currentBlock
    highestBlock
  }
}
```

If it returns an error message `TypeError: Failed to fetch`, Geth is not running correctly.
If you're syncing with a testnet or the mainnet, you're supposed to see something like this 

```json
{
  "data": {
    "syncing": {
      "currentBlock": "0x142381",
      "highestBlock": "0x143a94"
    }
  }
}
```

And if you're lucky and followed the instructions, you'll see this
```json
{
  "data": {
    "syncing": null
  }
}
```

Now we're ready to explore.

## Our first queries: get the balance

I said this is simple database that maps big integers to numbers, so let's
write the Hello-world program of key-values stores, query the value of a given key.
Here's the query

```graphql
query hello_world_ish {
	block {
		account(address: "0x1234567890123456789012345678901234567890") {
			balance
		}
	}
}
```

It looks quite involved for a hello world, so let's go line by line.

```graphql
query hello_world_ish { 
    ... 
}
```

This is GraphQL's way to give a name to the query. It's optional, a plain `block { ... }` would perfectly work.  I find it worth adding an extra line, so I can easily jump to a certain query in my IDE.

```graphql
    block { 
        ... 
    }
```

Blocks are the building blocks of every blockchain, hence the name.  Most data are stored in a block.  We'll revisit this concept in later sections.

```graphql
        account(address: "0x1234567890123456789012345678901234567890") {
            balance
        }
```

As I said the Ethereum blockchain is a weird key-value store that maps a
large random integer to a number. It actually works like a ledger at the bank
that maps an account number to its balance.

In the case of a bank, you identify an account by its account number. Its
format varies from bank to bank. For international transfers people use the
IBAN number which has a standard format. You have no control over your
account number, you're assigned one when you open an account.

Similarly to a bank account, you have no control over your Ethereum
accountaddress, it's generated randomly. But you do have some control over
the balance of your account. You can transfer money from your account to
another one which decreases your balance. You can also let people know about
your account number and ask them to send you money which would increase your
balance. If they eventually send money or not is up to them.

What this query does is ask for the balance of an account whose number is
given as `address`. The account address is internally stored as a 20-byte
integer which is a lot larger than a built-in integer datatype could hold, so
it's encoded as a 40 character long hexadecimal string. To show it's a
hexadecimal number, the string is prefixed with "0x". If you want to get the
underlying decimal number, you can convert it using any decent programming
language, or use an [online hexa to decimal
converter](https://www.danvk.org/hex2dec.html). Be careful, though, not every
online converter supports arbitrary precision.

Now run the above query.  This is the result you'll get,

```json
{
  "data": {
    "block": {
      "account": {
        "balance": "0x0"
      }
    }
  }
}
```

You may notice two things. First, the balance is returned as a 0x-prefixed
string, not as a plain number. Most users are not ether-millionaires but the
system was designed to accommodate larger numbers as balance. Second, and we
don't need a converter to tell, the balance is zero. You may wonder if the
test data you imported at the beginning contains this special account with a
zero balance. The answer is no, there's no such account in the database. When
you query the balance of a non-existent account, you'll always get zero, not
an error. We'll see later why this is implemented this way and how to tell if
an account exists.

Allrighty, you say, now show me an account that has money on it.  Here we go,

```graphql
query balance {
	block {
		account(address: "0xa94f5374fce5edbc8e2a8697c15331677e6ebf0b") {
			balance
		}
	}
}
```

It's the same query as before, the only difference is the account address. If
you execute it, you'll get back a similar json as before, but with `balance:
"0x340ab63a0215af0d"`. Convert it to decimal in one of the ways mentioned
above, and you'll see the balance is 3750009999998693133.

## Use variables

If you catch yourself replacing a parameter of the same GraphQL query, like
the address in this case, GraphQL allows for abstracting it out using
variables.  We want to specify the address outside the query itself.

Since GraphQL is a strongly typed language, we have to find out first the
type of the `address` field. Hover your mouse over that line of the code and
a tooltip will appear saying `Block.account(address: Address!)`. It contains
the relevant information that the type of address is `Address!`. Yes, it's
represented as a string, but we want to distinguish between an address and a
balance, however both are represented as strings. If you click on `Address!`
in the tooltip, the documentation will take you to a page that explains
"Address is a 20 byte Ethereum address, represented as 0x-prefixed
hexadecimal."

Now you know enough to enter this query in the query field of your editor

```graphql
query balance_with_arg($the_address:Address!) {
	block {
		account(address: $the_address) {
      balance
    }
  }
}
```

The first line contains the name of the query as earlier. The name is still
optional, but you can't omit the `query` keyword this time. The variable name
is prefixed with a $-sign and its type is `Address!` as found out earlier.
The exclamation mark after the type name means it's mandatory. If you omit
the exclamation mark in this line, the IDE will underline the variable
suggesting it's an error to use `Address` where `Address!` is required. In
other words, you can't allow null here.

The line `account(address: $the_address)` uses the variable that's defined in
the first line and will be fed to the query. You can reuse the same variable
at multiple places in the query.

Executing the query will return an error message "got null for non-null." We
managed to shut up the type-checker, but we haven't provided a value for
`$the_address`. In order to do that, open the "Query variables" field in the
bottom left corner of your browser IDE. Variables are given as a flat json
object where the key is the name of the variable (without the $-sign). Copy
this into the variables field:

```json
{
  "the_address": "0xa94f5374fce5edbc8e2a8697c15331677e6ebf0b"
}
```

Executing the query now returns the same result as in the previous case where
we specified the address directly in the query.

---

In this part we covered how to install an Ethereum client (Geth), how to
interact with it using GraphQL, and how to get the balance of an account. The
query was embedded on a `block`, I promised to tell you later why. In the
next installment we'll find out what blocks are and what interesting
properties they have.